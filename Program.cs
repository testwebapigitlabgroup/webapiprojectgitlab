using Microsoft.EntityFrameworkCore;
using TestingSonarcloud.Repository.Contracts;
using TestingSonarcloud.Repository;
using TestingSonarcloud.Data;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<Riskdbb28122023Context>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("RiskDBConnection"))
);
// Add services to the container.
builder.Services.AddScoped<IAttachmentRepository, AttachmentRepository>();

builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
