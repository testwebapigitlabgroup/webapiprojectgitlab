﻿using TestingSonarcloud.Data;
using TestingSonarcloud.Repository.Contracts;
using Microsoft.EntityFrameworkCore;

namespace TestingSonarcloud.Repository
{
    public class AttachmentRepository : IAttachmentRepository
    {
        private readonly Riskdbb28122023Context riskDBContext;
        public AttachmentRepository(Riskdbb28122023Context riskDbcontext)
        {
            this.riskDBContext = riskDbcontext;
        }
        public async Task<AttachmentTable> AddAttachment(AttachmentTable entity)
        {
            entity.DateCreated = DateTime.UtcNow;
            entity.DateModified = DateTime.UtcNow;

            var result = await this.riskDBContext.AttachmentTables.AddAsync(entity);

            await riskDBContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<AttachmentTable> DeleteAttachment(int id)
        {
            var item = await this.riskDBContext.AttachmentTables.FindAsync(id);

            if (item != null)
            {
                this.riskDBContext.AttachmentTables.Remove(item);
                await this.riskDBContext.SaveChangesAsync();
            }

            return item;
        }

        public async Task<AttachmentTable> GetItem(int id)
        {
            var item = await this.riskDBContext.AttachmentTables
                                    .FindAsync(id);

            return item;
        }

        public async Task<IEnumerable<AttachmentTable>> GetItems()
        {
            var list = await this.riskDBContext.AttachmentTables.ToListAsync();
            return list;
        }

        public async Task<IEnumerable<AttachmentTable>> GetItemsByRequestID(int RequestID)
        {
            var item = await this.riskDBContext.AttachmentTables
                                             .Where(p => p.RequestId == RequestID)
                                             .ToListAsync();
            return item;
        }

        public async Task<IEnumerable<AttachmentTable>> GetItemsCreatedBy(string CreatedByUserID)
        {
            var item = await this.riskDBContext.AttachmentTables
                                             .Where(p => p.CreatedBy == CreatedByUserID)
                                             .ToListAsync();
            return item;
        }

        public async Task<AttachmentTable> UpdateAttachment(int id, AttachmentTable entity)
        {
            AttachmentTable item = await riskDBContext.AttachmentTables.FindAsync(id);
            entity.DateCreated = item.DateCreated;
            entity.DateModified = DateTime.UtcNow;
            riskDBContext.Entry(item).CurrentValues.SetValues(entity);

            var result = await riskDBContext.SaveChangesAsync();
            return item;
        }

        

        
    }
}
